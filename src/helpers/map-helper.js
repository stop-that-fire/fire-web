import FireIcon from '../assets/fire-marker.svg'

export default class Map {
  constructor(params) {
    this.el = params.el
    this.center = this.getLocation()
    this.incidents = []
    this.markers = []
    this.map = new google.maps.Map(this.el, {
      center: {lat: 0, lng: 0},
      zoom: 8,
      styles,
    })
  }

  render() {
    this.map = new google.maps.Map(this.el, {
      center: this.center,
      zoom: 8,
      styles,
    })
    this.initMap()
  }

  initMap() {
    // this.map.addListener('zoom_changed', () => this.reRenderMarkers())
  }

  clearMarkers() {
    this.markers = this.markers.map(marker => marker.setMap(null))
    this.markers = new Array()
  }

  reRenderMarkers() {
    this.clearMarkers()
    this.incidents.map(incident => this.addMarker(incident))
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition(position => {
        this.showPosition(position)}, error => this.showError(error))
    }
  }

  showPosition(position) {
    this.center = {
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    }
    sessionStorage.setItem('userLat', this.center.lat)
    sessionStorage.setItem('userLng', this.center.lng)
    this.error = null
    this.render()
  }

  showError(error) {
    this.center = {
      lat: 0,
      lng: 0,
    }
    sessionStorage.setItem('userLat', this.center.lat)
    sessionStorage.setItem('userLng', this.center.lng)
    this.error = error.message
    this.render()
  }

  addMarker(point) {
    this.incidents.push(point)
    this.markers.push(new google.maps.Marker({
      position: { lat: point.latitude, lng: point.longitude},
      map: this.map,
      icon: {
        url: FireIcon,
        origin: new google.maps.Point(0, 0),
        scaledSize: new google.maps.Size(25, 25),
        anchor: new google.maps.Point(0, 0),
      },
    }))
  }

  goToLocation(incident) {
    this.markers.map(marker => marker.setAnimation(null))
    this.map.panTo({ lat: incident.latitude, lng: incident.longitude })
    this.map.setZoom(11)
    const marker = this.markers.find(marker => marker.position.lat() === incident.latitude)
    marker.setAnimation(google.maps.Animation.bn)
  }

  removeMarker(marker) {
    marker.setMap(null)
  }
}

const styles =  [
  { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
  { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
  { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }]
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }]
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [{ color: '#263c3f' }]
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#6b9a76' }]
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [{ color: '#38414e' }]
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [{ color: '#212a37' }]
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#9ca5b3' }]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [{ color: '#746855' }]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [{ color: '#1f2835' }]
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#f3d19c' }]
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [{ color: '#2f3948' }]
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#d59563' }]
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [{ color: '#17263c' }]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [{ color: '#515c6d' }]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [{ color: '#17263c' }]
  }
]