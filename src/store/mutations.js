import Incident from '../models/incident'
import { distanceBetweenPoints } from '../utils/coords-helper' 

export default {
  SET_INCIDENTS: (state, incidents) => {
    const userLocation = { lat: Number(sessionStorage.getItem('userLat')), lng: Number(sessionStorage.getItem('userLng')) }
    let distances = incidents
      .map(distance => distanceBetweenPoints(userLocation.lng, userLocation.lat, distance.longitude, distance.latitude))
    let unsortedResult = incidents.map((incident, index) => new Incident({ ...incident, distanceWithUser: distances[index] }))
    distances = distances.sort()
    let sortedResult = []
    distances.forEach(distance => {
      sortedResult.push(unsortedResult.find(incident => incident.distanceWithUser === distance))
    })
    state.incidents = sortedResult
  }
}