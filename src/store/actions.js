export default {
  storeIncidents: (context, incidents) => {
    context.commit('SET_INCIDENTS', incidents)
  }
}