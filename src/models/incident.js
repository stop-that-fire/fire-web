import deserializer from '../utils/deserializer'

export default class Incident {
  constructor(params) {
    this.params = deserializer(params)
    for (const [key, value] of Object.entries(this.params)) {
      this[key] = value
    }
    this.formatAcqTime()
  }

  formatAcqTime() {
    this.acqTime = `0${this.acqTime}`.slice(-4)
    this.acqTime = `${this.acqTime.slice(0, 2)}:${this.acqTime.slice(2, 4)}`
  }
}