import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import LoadScript from 'vue-plugin-load-script'
import { GOOGLE_API_KEY } from './constants/api-keys'
import _ from 'lodash'

Vue.prototype._ = _

Vue.config.productionTip = false

Vue.use(LoadScript)

Vue.loadScript(`https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=drawing,geometry,places,visualization`)
  .then(() => {
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  })
