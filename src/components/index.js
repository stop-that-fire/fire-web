import Map from './map.vue'
import Drawer from './drawer.vue'
import ColoredNumber from './colored-number.vue'

export {
  Map,
  Drawer,
  ColoredNumber,
}