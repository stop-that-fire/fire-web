export const distanceBetweenPoints = (srcLon, srcLat, destLon, destLat) => {
  const R = 6371 // Radius of the earth in km
  var dLat = toRad(destLat - srcLat)
  var dLon = toRad(destLon - srcLon)
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRad(srcLat)) * Math.cos(toRad(destLat)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

const toRad = source => {
  let pi = Math.PI
  return source * (pi/180)
}