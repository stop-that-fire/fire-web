const deserializer = object => {
  let result = {}
  for (let [key, value] of Object.entries(object)) {
    result[_.camelCase(key)] = value 
  }
  return result
}

export default deserializer