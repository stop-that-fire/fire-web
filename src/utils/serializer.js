const serializer = object => {
  let result = {}
  for (let [key, value] of Object.entries(object)) {
    result[_.snakeCase(key)] = value
  }
  return result
}